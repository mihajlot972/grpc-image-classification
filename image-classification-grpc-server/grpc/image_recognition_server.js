var PROTO_PATH = './grpc/protos/image_recognition.proto';
// var imageClassification = require('./image_recognition')
var fs = require('fs');
var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
  PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  });
var image_recognition_proto = grpc.loadPackageDefinition(packageDefinition).imagerecognition;
import imageRecognition from './image_recognition'


function recognize(call, callback) {
  var filename;
  var buffers = [];

  call.on('data', function (event) {
    filename = event.filename
    buffers.push(event.image)
  })

  call.on('end', async function () {
    fs.writeFileSync(`./grpc/images/${filename}.jpg`, Buffer.concat(buffers), 'binary');

    let recognizeImage = await imageRecognition(__dirname + `/images/${filename}.jpg`)
    console.log(recognizeImage)

    callback(null, {
      message: recognizeImage[0].className,
      code: 1
    })
  })
}

function sayHello(call, callback) {
  callback(null, {
    message: "good, it works"
  })
}

/**
 * Starts an RPC server that receives requests for the ImageRecognition service at the
 * sample server port
 */
function main() {
  var server = new grpc.Server();
  server.addService(image_recognition_proto.ImageRecognition.service, {
    recognize: recognize,
    sayHello: sayHello
  });
  server.bindAsync('0.0.0.0:50053', grpc.ServerCredentials.createInsecure(), () => {
    server.start();
    console.log("gRPC server is running...")
  });
}

main();