import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ImageClassificationModule } from './image-classification/image-classification.module';

@Module({
  imports: [ImageClassificationModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
