import { Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import { grpcClientOptions } from '../grpc-client.options';
import { ImageClassificationController } from './image-classification.controller';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'IMAGE_RECOGNITION',
        ...grpcClientOptions,
      },
    ]),
  ],
  controllers: [ImageClassificationController],
})
export class ImageClassificationModule {}