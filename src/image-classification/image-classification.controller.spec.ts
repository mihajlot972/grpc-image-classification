import { Test, TestingModule } from '@nestjs/testing';
import { ImageClassificationController } from './image-classification.controller';

describe('ImageClassificationController', () => {
  let controller: ImageClassificationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ImageClassificationController],
    }).compile();

    controller = module.get<ImageClassificationController>(ImageClassificationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
