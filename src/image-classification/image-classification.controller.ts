import { Controller, Get, Inject, OnModuleInit, Post, UploadedFile, Req, UseInterceptors } from '@nestjs/common';
import {
  ClientGrpc,
  GrpcStreamCall,
  GrpcStreamMethod,
} from '@nestjs/microservices';
import { FileInterceptor } from '@nestjs/platform-express';
import { Observable, ReplaySubject, Subject, Subscriber } from 'rxjs';
import * as fs from 'fs'

interface ImageRecognition {
  // recognize(data: HeroById): Observable<Hero>;
  // sayHelloAgain(upstream: Observable<HeroById>): Observable<Hero>;
  recognize: any
  sayHello: any
}


@Controller('image-classification')
export class ImageClassificationController implements OnModuleInit {

  private imageRecognitionService: ImageRecognition

  constructor(
    @Inject('IMAGE_RECOGNITION') private readonly client: ClientGrpc
  ) { }


  onModuleInit() {
    this.imageRecognitionService = this.client.getService<ImageRecognition>('ImageRecognition');
  }

  @Post("hello")
  async sayHelloMethod() {
    return await this.imageRecognitionService.sayHello({ message: "ZDRAVO DOKTORE" }).toPromise()
  }


  @Post("classify")
  @UseInterceptors(FileInterceptor("photo", { dest: "./uploads" }))
  async classifyImage(@UploadedFile() file, @Req() req) {



    let classifiedImage = await this.recognize(file)
    return { classifiedImage }
  }


  @GrpcStreamCall()
  async recognize(file) {

    let readStream = fs.createReadStream(file.path)
    const fileObservable = new Observable(subscriber => {

      readStream.on('data', function (chunk) {
        subscriber.next({ image: chunk })
      })

      readStream.on('end', function () {
        subscriber.complete()
      })
    })

    let classifiedImage = await this.imageRecognitionService.recognize(fileObservable).toPromise()
    return classifiedImage
  }

  async getImage(image) {
    return await this.imageRecognitionService.recognize({ image: image }).toPromise()
  }
}
