import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const grpcClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: "0.0.0.0:50053",
    package: 'imagerecognition',
    protoPath: join(__dirname, './image-classification/image-classification.proto'),
  },
};
